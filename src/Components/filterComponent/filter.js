import { useState } from "react";
import { useDispatch } from "react-redux";
import { Col, Row, Input, Label, FormGroup, Button } from "reactstrap";
import getCondition from "../../actions/product.action";

const fetchApi = async(url, body) =>{
    const response = await fetch(url,body);
    const data = await response.json();
    return data;
}
export default function FilterTable () {
    const [minPrice, getMinPrice ] = useState("");
    const [maxPrice, getMaxPrice] =useState("");
    const [categories, getCategories] = useState("");
    const [type, getType] = useState("");
    const condition = {};
    const dispatch = useDispatch();
    const getValue = (e) =>{
            getType(e.target.value)
    }
    const getValueDescription = (e) =>{
        getCategories(e.target.value);
        
    }
    const callApiFilter = () =>{
        if(categories){
            condition.description = categories;
            dispatch(getCondition(condition));
        }
        else{
            condition.description = ""
        }
        if(type){
            condition.type = type;
            dispatch(getCondition(condition));
        }
        else{
            condition.type = ""
        }
        if(minPrice){
            condition.minPrice = minPrice;
            dispatch(getCondition(condition));
        }
        else{
            condition.minPrice = ""
        }
        if(maxPrice){
            condition.maxPrice = maxPrice;
            dispatch(getCondition(condition));
        }
        else{
            condition.maxPrice = ""
        }
        // console.log(condition);
        // dispatch(getCondition(condition));
    }
    return (
        <>
            <Row className="mb-5">
                <h5>Description:</h5>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" value="LivingRoom" name="radio1" onChange={getValueDescription}/>
                    <Label check>
                    Living Room
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" value="BedRoom" name="radio1" onChange={getValueDescription}/>
                    <Label check>
                    Bed Room
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" value="LoftRoom" name="radio1" onChange={getValueDescription}/>
                    <Label check>
                    Loft Room
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" value="Kitchen" name="radio1" onChange={getValueDescription}/>
                    <Label check>
                    Kitchen
                    </Label>
                </FormGroup>
            </Row>
            <Row className="mb-5">
                <h5>Price:</h5>
                <div className="d-flex col align-items-center justify-content-center">
                    <Col xs="2"> 
                        <p style={{margin:0}}>Min:</p> 
                    </Col>
                    <Col xs="4">
                        <Input style={{width:"80%"}} onChange={(e) => getMinPrice(e.target.value)}/>
                    </Col>
                    <Col xs="2"> 
                        <p style={{margin:0}}>Max:</p> 
                    </Col>
                    <Col xs="4">
                        <Input style={{width:"80%"}} onChange={(e) => getMaxPrice(e.target.value)}/>
                    </Col>
                </div>
            </Row>
            <Row>
                <h5>Type:</h5>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" value="Table" name="radio2" onChange={getValue}/>
                    <Label check>
                    Table
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" name="radio2" value="Clother Deck" onChange={getValue}/>
                    <Label check>
                    Clother Deck
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" name="radio2" value="Chair" onChange={getValue}/>
                    <Label check>
                    Chair
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" name="radio2" value="Sofa" onChange={getValue}/>
                    <Label check>
                    Sofa
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" name="radio2" value="Tivi Table" onChange={getValue}/>
                    <Label check>
                    Tivi Table
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" name="radio2" value="Bed" onChange={getValue}/>
                    <Label check>
                    Bed
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" name="radio2" value="Mirror" onChange={getValue}/>
                    <Label check>
                    Mirror
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" name="radio2" value="Book Deck" onChange={getValue}/>
                    <Label check>
                    Book Deck
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" name="radio2" value="Oven rack" onChange={getValue}/>
                    <Label check>
                    Oven rack
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" name="radio2" value="Storage" onChange={getValue}/>
                    <Label check>
                    Storage
                    </Label>
                </FormGroup>
                <FormGroup
                    check
                    inline
                >
                    <Input type="radio" name="radio2" value="Cup" onChange={getValue}/>
                    <Label check>
                    Cup
                    </Label>
                </FormGroup>
                <Button style={{marginTop: 10,marginBottom: 50, backgroundColor:"#d1c286", border:0, width:"60%"}} onClick={callApiFilter}>Filter Product</Button>
            </Row>
        </>
    )
}