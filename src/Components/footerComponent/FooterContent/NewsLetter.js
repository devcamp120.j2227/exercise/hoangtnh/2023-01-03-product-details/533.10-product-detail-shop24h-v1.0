import { Button, Col, Input, Row } from "reactstrap";
import '@fortawesome/fontawesome-free/css/all.min.css';

const NewsLetter = () =>{
    return (
        <div className="d-flex align-items-start flex-column">
            <Row className="mb-4">
                <h4>Newsletter</h4>
            </Row>
            <Row>
                <Input placeholder="Enter your email"/>
            </Row>
            <Row>
                <Button color="success" style={{width:"200px",marginTop:"10px"}}>
                    SUBSCRIBE
                </Button>
            </Row>
            <Row className="mt-5">
                <Col><h3><i type="button" className="fa-brands fa-facebook"></i></h3></Col>
                <Col><h3><i type="button" className="fa-brands fa-twitter"></i></h3></Col>
                <Col><h3><i type="button" className="fa-brands fa-linkedin"></i></h3></Col>
                <Col><h3><i type="button" className="fa-brands fa-youtube"></i></h3></Col>
            </Row>
        </div>
    )
}
export default NewsLetter;