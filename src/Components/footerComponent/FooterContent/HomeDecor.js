import { Col, Row } from "reactstrap"
import img1 from "../../../assets/homedecor/3.jpg"
import img2 from "../../../assets/homedecor/4.jpg"
import img3 from "../../../assets/homedecor/6.jpg"
import img4 from "../../../assets/homedecor/8.jpg"

const HomeDecor = () =>{
    return (
        <>
        <div className="d-flex align-items-start flex-column">
            <Row className="mb-4">
                <h4>Home Decor</h4>
            </Row>
        </div>
        <Row className="mb-5">
            <Row>
                <Col style={{paddingRight:"0px"}}>
                    <img id="img-div-footer" src={img4} alt=""/>
                    <img id="img-div-footer" src={img2} alt=""/>
                </Col>
                <Col style={{paddingLeft:"0px"}}>
                    <img id="img-div-footer" src={img3} alt=""/>
                    <img id="img-div-footer" src={img1} alt=""/>
                </Col>
            </Row>
        </Row>
        </>
    )
}
export default HomeDecor;