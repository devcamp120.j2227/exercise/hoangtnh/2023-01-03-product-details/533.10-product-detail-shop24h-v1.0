import { useState, useEffect } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, Input, FormGroup, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import '@fortawesome/fontawesome-free/css/all.min.css';
//import các lệnh đăng nhập gg
import auth from '../../firebase';
import { GoogleAuthProvider, signInWithPopup, signOut, onAuthStateChanged } from 'firebase/auth';
import { useDispatch } from 'react-redux';
import { loginUserInformation } from '../../actions/product.action';
//đăng nhập bằng tài khoản gg trên react
const provider = new GoogleAuthProvider();
function LoginModal(args) {
    //khởi tạo các trạng thái ban đầu
  const [open, setOpen] = useState(false);
  const [dropdownOpen, setDropdownOpen] = useState(false);
    //thao tác khi bấm vào avatar
  const toggleAvatar = () => setDropdownOpen(!dropdownOpen);
  const toggle = () => setOpen(!open);
    
    //login gg khởi tạo giá trị user ban đầu
    const [user, setUser] = useState(null);
    //xử lý nút đăng nhập bằng tài khoản gg
    const onLogInButton = () =>{
        signInWithPopup(auth, provider)
        .then((result) =>{
            setUser(result.user)
        })
        .catch((error)=>{
            console.error(error)
        });
        setOpen(!open)
    }
    //log out 
    const logoutGoogle = () => {
        signOut(auth)
          .then(() => {
            setUser(null);
          })
          .catch((error) => {
            console.error(error);
          })
      }
    //call dispatch để truyền dữ liệu user vào redux
    const dispatch = useDispatch();

    //giữ lại trạng thái user đăng nhập khi reload trang
    useEffect(() => {
        onAuthStateChanged(auth, (result) => {
            setUser(result);
            //truyền user vào redux để thao tác trên check cart
            dispatch(loginUserInformation(result));
            localStorage.setItem("user", JSON.stringify(result));
        })
      }, [user])
  return (
    <>  
        {user? 
        <Row>
            <Col sm={3} className="d-flex align-items-center">
                <Dropdown nav isOpen={dropdownOpen} toggle={toggleAvatar} style={{listStyleType:"none"}}>
                    <DropdownToggle nav >
                        <img src={user.photoURL} referrerpolicy="no-referrer" width={30} style={{borderRadius: "50%"}}/>
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem>
                            <Button style={{backgroundColor:"#d1c286"}} onClick={logoutGoogle}>Log out</Button>
                        </DropdownItem>
                    </DropdownMenu>
                </Dropdown>
            </Col>
        </Row> 
            : 
        <div>
            <i className="fa-solid fa-user" onClick={toggle}></i>
            <Modal isOpen={open} toggle={toggle} {...args}>
                <ModalHeader toggle={toggle} style={{color:"#d1c286"}}>Sign In</ModalHeader>
                <ModalBody >
                    <Row >
                        <Col className='d-flex justify-content-center' sm={12}>
                            <Button color='danger' style={{ width:"80%",borderRadius:"50px"}}
                                onClick={onLogInButton}>
                            <i className="fa-brands fa-google"/> Sign In with <b>Google</b> 
                            </Button>
                        </Col>
                        <Col className='d-flex justify-content-center' sm={12}>
                        <div><hr style={{width:"150px",border:"1px solid black"}}/></div>
                        </Col>
                        <Col sm={12} className='d-flex flex-column align-items-center'>
                            <FormGroup style={{width:"80%"}} sm={12}>
                                <Input
                                name="username"
                                placeholder="Username"
                                style={{borderRadius:"50px"}}
                                />
                            </FormGroup>
                            <FormGroup style={{width:"80%"}}>
                                <Input
                                name="password"
                                placeholder="Password"
                                type="password"
                                style={{borderRadius:"50px"}}
                                />
                            </FormGroup>
                            <Button color='success' style={{ width:"80%",borderRadius:"50px"}}>
                                Sign In  
                            </Button>
                        </Col>
                    </Row>
                </ModalBody>
                <ModalFooter>
                    <Col className='text-center'>
                        <p> Don't have an account?  <p  style={{color:"green"}}>Sign up here</p></p>
                    </Col>
                </ModalFooter>
        </Modal>
      </div>
    }
    </>  
    
  );
}

export default LoginModal;