import { useDispatch, useSelector } from 'react-redux';
import { DialogActions, Button, DialogContent, Typography, Dialog, Grid, DialogTitle } from '@mui/material';
import { changeCartStatus } from '../../actions/product.action';
import { useNavigate } from 'react-router-dom';

const ShoppingCart = () =>{
    const {user, cartBag, checkCartStatus} =  useSelector((reduxData)=>
        reduxData.productReducer
    );
    //điều hướng url
    const navigate = useNavigate();
    //sử dụng dispatch dùng dữ liệu của redux
    const dispatch= useDispatch();
    //đóng mở dialog
    const handleClose = () => dispatch(changeCartStatus(!checkCartStatus));
    //tính tổng tạm thời tiền phải thanh toán hiển thị ra dialog
    const total = cartBag.reduce((sumary, item)=> sumary+ item.Product.PromotionPrice*item.quantity, 0);
    //khi bấm nút kiểm tra giỏ hàng
    const onBtnCheckCartHandler =() =>{
        //nếu chưa đăng nhập thì chuyển hướng sang trang đăng nhập
        if(user === null){
            dispatch(changeCartStatus(!checkCartStatus));
            navigate("/Login");
        }
        //nếu đăng nhập rồi thì chuyển hướng về trang giỏ hàng của người dùng
        else{
            navigate("/Cart");
            dispatch(changeCartStatus(!checkCartStatus));
        }
    }
    return (
        <>
           <Dialog open= {checkCartStatus} scroll= "paper" component="form" onClose={handleClose}  fullWidth maxWidth="md">
                    <DialogTitle>
                        <Typography id="transition-modal-title" variant="h4" component="h2">
                            Your Cart
                        </Typography>
                    </DialogTitle>
                    <DialogContent dividers='paper'>
                        {cartBag.map((value,index)=>{
                            return (
                                <Typography sx={{ mt: 2 }} key={index}>
                                    <Grid
                                        id="transition-modal-description"
                                        container
                                        direction="row"
                                        justifyContent="flex-start"
                                        alignItems="center"
                                        spacing={2}
                                        >   <Grid item xs={3}><img src={value.Product.ImageUrl} style={{height:"100%", width:"100%", marginTop:"-10%"}}/></Grid>
                                            <Grid item xs={5}><b><p>{value.Product.Name}</p></b></Grid>
                                            <Grid item xs={2}> <p>Price: <b>${value.Product.PromotionPrice * value.quantity}</b></p></Grid>
                                            <Grid item xs={2}><p>Quantity: <b>{value.quantity}</b></p></Grid>
                                        </Grid>
                                </Typography>
                                )
                            }
                        )}
                    </DialogContent>
                    <DialogActions>
                        <Grid
                            container
                            direction="row"
                            justifyContent="flex-start"
                            alignItems="center"
                            spacing={2}
                            > 
                            <Grid item marginLeft={6}> <h5 style={{margin:0}}>Temporary: <b style={{color:"#d1c286"}}>${total}</b></h5></Grid>
                        </Grid>
                        <Button  onClick={handleClose} variant="outlined" style={{color:"black", border:" 1px black"}}>Close</Button>
                        <Button variant="contained" style={{backgroundColor:"#d1c286", width:"30%"}} onClick={()=>onBtnCheckCartHandler()}>Check Cart</Button>
                    </DialogActions>
            </Dialog>
    </>
    )
}
export default ShoppingCart;