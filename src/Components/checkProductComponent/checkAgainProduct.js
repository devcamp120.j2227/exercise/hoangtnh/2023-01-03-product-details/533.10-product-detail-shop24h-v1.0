import { Container, Grid, TableContainer, Table, TableRow, TableCell, TableBody, Paper, TableHead,Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import ClearIcon from '@mui/icons-material/Clear';
import { onBtnMinusHandler, onBtnPlusHandler, onBtnRemoveProduct } from "../../actions/product.action";
import { useNavigate } from "react-router-dom";
import { ThemeProvider, createTheme } from '@mui/material/styles'; 

const theme = createTheme({
  typography: {
    allVariants: {
      fontFamily: 'Comfortaa',
      textTransform: 'none',
      fontSize: 16,
    },
  },
});
const CheckProductAndPayment = () =>{
    const {cartBag, user} = useSelector((reduxData) =>
        reduxData.productReducer
    );
    const navigate = useNavigate();
    const dispatch = useDispatch();
        //hàm xử lý nút tăng sản phẩm
        const onBtnPlus = (e) =>{
            dispatch(onBtnPlusHandler(e.Product._id));
        };
        //hàm xử lý nút giảm sản phẩm
        const onBtnMinus = (e) =>{
            dispatch(onBtnMinusHandler(e.Product._id));
        };
        //hàm xử lý nút xóa sản phẩm
        const onBtnDelProduct = (e) =>{
            dispatch(onBtnRemoveProduct(e.Product._id));
        }
        const total = cartBag.reduce((sumary, item)=> sumary+ item.Product.PromotionPrice*item.quantity, 0);
    return(
        <Container md={12} sm={12} lg={12} xs={12}>
            <Grid container direction="row"
                    justifyContent="flex-start"
                    alignItems="center"
                    spacing={2}
                    margin={5}>
                <Grid item>
                    <b>COSY-HOME</b>
                </Grid>
                <Grid item>
                    |
                </Grid>
                <Grid item>
                    SHOPPING CART
                </Grid>
            </Grid>
            {user?
            <ThemeProvider theme={theme}>
                <TableContainer component={Paper} className="mb-4" md={12} sm={12} lg={12} xs={12}  >
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left"><b>Products</b></TableCell>
                                <TableCell align="left" ><b>Price</b></TableCell>
                                <TableCell align="left" ><b>Quantity</b></TableCell>
                                <TableCell align="left" ><b>Total</b></TableCell>
                                <TableCell align="left" ></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {cartBag.map((value, index) => (
                            <TableRow
                            key={index}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            md={12} sm={12} lg={12} xs={12}>
                            <TableCell component="th" scope="row" align="left"style={{width:"80%"}}><img src={value.Product.ImageUrl} style={{height:"auto", width:"100px"}}/> &nbsp;&nbsp;{value.Product.Name}</TableCell>
                            <TableCell align="left"style={{width:"100%",paddingRight:0  }}>$ {value.Product.PromotionPrice}</TableCell>
                            <TableCell align="left" style={{width:"100%"}}>
                                <Grid container direction="row"
                                    justifyContent="flex-start"
                                    alignItems="center" sx={{width:"100px"}}>
                                        <Grid item>
                                            <button style={{margin:"5px",padding:"2px 11px", borderRadius:"50%", border:"0px", backgroundColor:"#d1c286", color:"white"}}
                                                onClick={() => onBtnMinus(value)}>-</button>
                                        </Grid>
                                        <Grid item>
                                            {value.quantity <= 0? 1:value.quantity}
                                        </Grid>
                                        <Grid item>
                                            <button style={{margin:"5px",padding:"2px 9px", borderRadius:"50%", border:"0px", backgroundColor:"#d1c286", color:"white"}}
                                                onClick={() => onBtnPlus(value)}>+</button>
                                        </Grid>
                                    </Grid>      
                            </TableCell>
                            <TableCell align="left" style={{width:"100%",paddingRight:0}}> $ {value.Product.PromotionPrice * value.quantity}</TableCell>
                            <TableCell align="left"><ClearIcon type="button" onClick= {()=> onBtnDelProduct(value)}/></TableCell>
                            </TableRow>
                        ))} 
                        </TableBody>
                    </Table>
                </TableContainer>
                <Grid>
                    <Button style={{backgroundColor:"gray", color: "white"}} onClick={()=>navigate("/Categories/More")}> Continue Shopping</Button>
                </Grid>
                <Grid container  style={{backgroundColor:"whiteSmoke", padding:10, width:"50%", margin:"auto", marginBottom:20}}
                        direction="column" md={12} sm={12} lg={12} xs={12}>
                    <Grid item  textAlign="center" marginBottom={5}>
                        <h4>Cash total</h4>
                    </Grid>
                    <Grid container sx={{width:"100%"}}
                    direction="row"
                    marginBottom={5}>
                        <Grid item textAlign="center" xs={6}>
                            <b>Customer:</b>
                        </Grid>
                        <Grid item textAlign="center"xs={6}>
                            <b style={{color:"#d1c286"}}> {user.displayName}</b>
                        </Grid>
                    </Grid>
                    <Grid container sx={{width:"100%"}}
                    direction="row"
                    marginBottom={5}>
                        <Grid item textAlign="center" xs={6}>
                            <b>Total:</b>
                        </Grid>
                        <Grid item textAlign="center"xs={6}>
                            <b style={{color:"#d1c286"}}>$ {total}</b>
                        </Grid>
                    </Grid>
                    <Grid container
                    direction="row"
                    justifyContent="center">
                        <Button style={{backgroundColor:"#7fad39", color:"white", padding:"10px 50px"}}><b>Process To Check out</b></Button>
                    </Grid>
                </Grid>
                </ThemeProvider>
        : <Grid textAlign="center" margin={"15%"} style={{color:"#d1c286"}}>You need to sign in for completing check out your cart</Grid>}
        
        </Container>
    )
}
export default CheckProductAndPayment;