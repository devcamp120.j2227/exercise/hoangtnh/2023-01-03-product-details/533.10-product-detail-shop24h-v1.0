import { ADD_PRODUCT_TO_CART, CHECK_CART_STATUS, DELETE_PRODUCT_IN_CART, EDIT_MINUS_QUANTITY_PRODUCT, EDIT_PLUS_QUANTITY_PRODUCT, GET_CONDITION_API, LOGIN_USER, PRODUCT_CLICK_HANDLER, QUANTITY_PRODUCT_HANDLER } from "../constants/product.action";
const item = JSON.parse(localStorage.getItem("item")) || [];
const userName = JSON.parse(localStorage.getItem("user")) || [];
const initialState = {
    condition: null,
    status: false,
    productDetail: null,
    quantity: 0,
    cartBag: item,
    numberProduct: item.length,
    user: userName.displayName,
    checkCartStatus: false
}
export default function productReducer(state = initialState, action){
        switch (action.type) {
            case GET_CONDITION_API:
                state.condition = action.payload;
                state.status = true
                break;
            case PRODUCT_CLICK_HANDLER:
                state.productDetail = action.payload;
                state.status = false;
                state.quantity = 0;
                break;
            case QUANTITY_PRODUCT_HANDLER:
                state.quantity =  action.payload;
                if(state.quantity < 0){
                    state.quantity = 0;
                    break;
                }
                break;
            case LOGIN_USER:
                state.user = action.payload;
                break;
            case ADD_PRODUCT_TO_CART:
                state.cartBag = action.payload
                state.numberProduct = action.payload.length
                break;
            
            case CHECK_CART_STATUS:
                state.checkCartStatus = !state.checkCartStatus;
                break;

            //khi nút tăng số lượng ấn truyền id của sản phẩm sang sau đó thay đổi quantity của sản phẩm tương ứng trong localStorage
            case EDIT_PLUS_QUANTITY_PRODUCT:
                const indexPlus = state.cartBag.findIndex(cardBag => cardBag.Product._id === action.payload)
                state.cartBag[indexPlus].quantity = state.cartBag[indexPlus].quantity + 1;
                localStorage.setItem("item",JSON.stringify(state.cartBag));
                console.log(state.cartBag)
                break;

            //khi nút giảm số lượng ấn truyền id của sản phẩm sang sau đó thay đổi quantity của sản phẩm tương ứng trong localStorage
            case EDIT_MINUS_QUANTITY_PRODUCT:
                const indexMinus = state.cartBag.findIndex(cardBag => cardBag.Product._id === action.payload)
                state.cartBag[indexMinus].quantity = state.cartBag[indexMinus].quantity - 1;
                if(state.cartBag[indexMinus].quantity <= 0){
                    state.cartBag[indexMinus].quantity = 1
                }
                localStorage.setItem("item",JSON.stringify(state.cartBag));
                console.log(state.cartBag)
                break;
            //khi xóa sản phẩm tìm theo id của sản phẩm sau đó cắt vị trí cần xóa tương ứng với id của sản phẩm
            case DELETE_PRODUCT_IN_CART:
                const indexRemove = state.cartBag.findIndex(cardBag => cardBag.Product._id === action.payload);
                state.cartBag.splice(indexRemove, 1);
                localStorage.setItem("item",JSON.stringify(state.cartBag));
                break;
            default:
                break;
        }
        console.log(state)
    return {...state}
}