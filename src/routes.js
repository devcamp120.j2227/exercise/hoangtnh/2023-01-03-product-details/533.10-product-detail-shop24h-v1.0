import Categories from "./Components/bodyComponent/bodyContent/Categories";
import CheckProductAndPayment from "./Components/checkProductComponent/checkAgainProduct";
import LoginForm from "./Components/modalComponent/loginForm";
import HomePageShop24h from "./pages/homePage";
import MoreProduct from "./pages/moreProducts";
import ProductDetails from "./pages/productDetail";


const routerList = [
    {path: "/", element: <HomePageShop24h/>},
    {path:"/Categories", element:<Categories/>},
    {path:"/Categories/More", element:<MoreProduct/>},
    {path:"/Categories/More/Details", element:<ProductDetails/>},
    {path:"/Login", element:<LoginForm/>},
    {path:"/Cart", element:<CheckProductAndPayment/>},

]
export default routerList;