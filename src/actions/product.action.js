import { ADD_PRODUCT_TO_CART, CHECK_CART_STATUS, DELETE_PRODUCT_IN_CART, EDIT_MINUS_QUANTITY_PRODUCT, EDIT_PLUS_QUANTITY_PRODUCT, GET_CONDITION_API, LOGIN_USER, PRODUCT_CLICK_HANDLER, QUANTITY_PRODUCT_HANDLER } from "../constants/product.action";

export default function getCondition  (data) {
    return{
        type: GET_CONDITION_API,
        payload: data
    }
};
export const getProductInfor = (data) =>{
    return {    
        type: PRODUCT_CLICK_HANDLER,
        payload: data
    }
}
export const quantityButtonHandler = (data) =>{
    return{
        type: QUANTITY_PRODUCT_HANDLER,
        payload: data
    }
}
export const addProductToCartHandler = (data) =>{
    return{
        type: ADD_PRODUCT_TO_CART,
        payload: data
    }
}
export const loginUserInformation = (data) =>{
    return{
        type: LOGIN_USER,
        payload: data
    }
}
export const changeCartStatus = () =>{
    return {
        type: CHECK_CART_STATUS
    }
}
export const onBtnPlusHandler = (id) =>{
    return {
        type: EDIT_PLUS_QUANTITY_PRODUCT,
        payload: id
    }
}
export const onBtnMinusHandler = (id) =>{
    return{
        type: EDIT_MINUS_QUANTITY_PRODUCT,
        payload: id
    }
}
export const onBtnRemoveProduct = (id) =>{
    return{
        type: DELETE_PRODUCT_IN_CART,
        payload: id
    }
}